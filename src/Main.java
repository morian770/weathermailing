import Objects.City;
import Pages.Bing;
import Pages.Google;
import Pages.RedMail_Ynet;
import Pages.Yahoo;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.Date;

public class Main {

    public static void main(String[] args) {
        System.setProperty("webdriver.chrome.driver", "/Volumes/Mendy/Mendy/weathermailing/otherSources/chromedriver");

        ////////////////////////////////////
        // Declarations
        ////////////////////////////////////

        WebDriver driver;
        WebDriverWait wait;

        Google google;
        Yahoo yahoo;
        Bing bing;
        RedMail_Ynet redMail_ynet;

        City telAviv;
        City jerusalem;
        String[] WEB_NAMES;

        String massage;
        String mailTo;
        String mailSubject;

        while (true) {

            ////////////////////////////////////
            // Initializations
            ////////////////////////////////////

            driver = new ChromeDriver();
            wait = new WebDriverWait(driver, Duration.ofSeconds(30));

            google = new Google(driver, wait);
            yahoo = new Yahoo(driver, wait);
            bing = new Bing(driver, wait);
            redMail_ynet = new RedMail_Ynet(driver, wait);

            telAviv = new City();
            jerusalem = new City();
            WEB_NAMES = new String[]{"Google", "Yahoo!", "Bing"};

            ////////////////////////////////////
            // Google
            ////////////////////////////////////

            google.openUrl();
            google.search("weather in tel aviv");
            telAviv.addToTemperatureArray(google.getTemperature());
            google.openUrl();
            google.search("weather in jerusalem");
            jerusalem.addToTemperatureArray(google.getTemperature());

            ////////////////////////////////////
            // Yahoo!
            ////////////////////////////////////

            yahoo.openUrl();
            yahoo.search("weather in tel aviv");
            yahoo.changeToCelsius();
            telAviv.addToTemperatureArray(yahoo.getTemperature());
            yahoo.openUrl();
            yahoo.search("weather in jerusalem");
            yahoo.changeToCelsius();
            jerusalem.addToTemperatureArray(yahoo.getTemperature());

            ////////////////////////////////////
            // Bing
            ////////////////////////////////////

            bing.openUrl();
            bing.search("weather in tel aviv");
            telAviv.addToTemperatureArray(bing.getTemperature());
            bing.openUrl();
            bing.search("weather in jerusalem");
            jerusalem.addToTemperatureArray(bing.getTemperature());

            massage = "The highest temperature found for Tel Aviv is: " + telAviv.getMaxTemperature() + "."
                    + "\nThe source is: " + WEB_NAMES[telAviv.getIndexOfMaxTemperature()] + "."
                    + "\n\n"
                    + "The highest temperature found for Jerusalem is: " + jerusalem.getMaxTemperature() + "."
                    + "\nThe source is: " + WEB_NAMES[jerusalem.getIndexOfMaxTemperature()] + ".";

            ////////////////////////////////////
            // Email
            ////////////////////////////////////

            mailTo = "spertivi@gmail.com";
            mailSubject = "Update for weather for current time: " + new Date().toString();
            Functions.sendEmail(mailTo, mailSubject, massage);

            ////////////////////////////////////
            // Red email Ynet
            ////////////////////////////////////

            redMail_ynet.openUrl();
            redMail_ynet.getTextarea().sendKeys(massage);
            redMail_ynet.getAgreeCheckbox().click();

            /*
            "Im not a robot" >> cant do this
            So, I cant submit!
            */
            // redMail_ynet.getSubmitButton().click();

            driver.quit();

            try {
                Thread.sleep(1000 * 60 * 60);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}