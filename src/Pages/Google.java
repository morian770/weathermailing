package Pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Google {
    @FindBy(name = "q")
    private WebElement searchBar;
    @FindBy(id = "wob_tm")
    private WebElement temp;
    private String url;
    private WebDriver driver;
    private WebDriverWait wait;


    public Google(WebDriver driver, WebDriverWait wait) {
        this.driver = driver;
        this.wait = wait;
        PageFactory.initElements(this.driver, this);
        this.url = "https://www.google.com/";
    }

    public void search(String textToSearch) {
        this.searchBar.sendKeys(textToSearch +  Keys.ENTER);
    }

    public int getTemperature() {
        return Integer.parseInt(wait.until(ExpectedConditions.visibilityOf(this.temp)).getText());
    }

    public void openUrl() {
        driver.get(this.url);
    }
}
