package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class RedMail_Ynet {
    private WebDriver driver;
    private WebDriverWait wait;
    private String url;

    @FindBy(css = "[placeholder=\"המידע\"]")
    private WebElement textarea;

    @FindBy(className = "button[type=\"submit\"]")
    private WebElement submitButton;

    @FindBy(className = "Checkbox_checkboxWrapper__j8NcL")
    private WebElement agreeCheckbox;

    public WebElement getAgreeCheckbox() {
        return agreeCheckbox;
    }

    public WebElement getTextarea() {
        return textarea;
    }

    public WebElement getSubmitButton() {
        return submitButton;
    }

    public RedMail_Ynet(WebDriver driver, WebDriverWait wait) {
            this.driver = driver;
            this.wait = wait;
            PageFactory.initElements(this.driver, this);
            this.url = "https://redmail.ynet.co.il/";
    }

    public void openUrl() {
        this.driver.get(this.url);
    }
}
