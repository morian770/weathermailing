package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Yahoo {
    @FindBy(name = "p")
    private WebElement searchBar;
    @FindBy(className = "currTemp")
    private WebElement temp;
    @FindBy(className = "unit")
    private WebElement unit;
    private String url;
    private WebDriver driver;
    private WebDriverWait wait;


    public Yahoo(WebDriver driver, WebDriverWait wait) {
        this.driver = driver;
        this.wait = wait;
        PageFactory.initElements(this.driver, this);
        this.url = "https://www.yahoo.com/";
    }

    public void search(String textToSearch) {
        this.searchBar.sendKeys(textToSearch + Keys.ENTER);
    }

    public int getTemperature() {
        return Integer.parseInt(wait.until(ExpectedConditions.visibilityOf(this.temp)).getText());
    }

    public void openUrl() {
        driver.get(this.url);
    }
    public void changeToCelsius () {
        wait.until(ExpectedConditions.visibilityOf(this.unit)).click();
    }
}
