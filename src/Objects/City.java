package Objects;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class City {
    private List<Integer> temperatureArray;

    public City() {
        this.temperatureArray = new ArrayList<>();
    }

    public void addToTemperatureArray(int tlvTemps) {
        this.temperatureArray.add(tlvTemps);
    }

    public List<Integer> getTemperatureArray() {
        return this.temperatureArray;
    }

    public int getMaxTemperature() {
        return Collections.max(this.temperatureArray);
    }

    public int getIndexOfMaxTemperature() {
        return this.temperatureArray.indexOf(this.getMaxTemperature());
    }
}
