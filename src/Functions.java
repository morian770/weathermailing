import dev.failsafe.internal.util.Assert;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

public class Functions {
    public static void sendEmail(String to, String subject, String body){
        try {
            String mailUrl = "https://script.google.com/macros/s/AKfycbygBOpTTkg-tWVphmczj8mOk0p7appWis6-9zUsnoeWYsvPFJRC7qW23t-POTPrzYAQdw/exec"
                    + "?to=" + to
                    + "&subject=" + URLEncoder.encode(subject, StandardCharsets.UTF_8)
                    + "&body=" + URLEncoder.encode(body, StandardCharsets.UTF_8);

            URL url = new URL(mailUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setConnectTimeout(10000);
            connection.setReadTimeout(10000);
            int statusCode = connection.getResponseCode();
            Assert.isTrue(statusCode == 200, "Status code is not 200");
            System.out.println(connection.getResponseMessage());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
